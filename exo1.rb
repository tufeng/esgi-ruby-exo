#Écrire un programme demandant à l'utilisateur de saisir deux chaînes de caractères au clavier.
#Voir si la première chaîne est deux fois plus grande que la deuxième et afficher un message approprié.

puts "Saisissez 2 chaines de caractères."
puts "1ere chaine :"
first = gets.chomp!
puts "2eme chaine :"
second = gets.chomp!

if(first.length === second.length * 2)
    puts "La première chaine est deux fois plus grande que la seconde"
else
    puts "La première chaine n'est pas deux fois plus grande que la seconde"
end