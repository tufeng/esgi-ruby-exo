#Ecrire un programme permettant de calculer la somme des nombres compris entre 1 et un entier demandé à l'utilisateur
#Exemple si l'utilisateur entre 10 : 1+2+3+4+5+6+7+8+9+10 --> 55

puts "enter a number :"
number = gets.to_i

i = 0
y = 0

while i <= number
    y=i+y
    i = i + 1
end

puts y