#Ecrire un programme qui demande à l'utilisateur de saisir 5 entiers stockés dans un tableau.
#Le programme doit ensuite afficher l'indice du plus grand élément, puis la moyenne des entiers.

puts "enter 5 number"

nums = Array.new

i = 0

while i < 5
    i = i + 1
    nums.push gets.to_i
end

puts "highest value index : #{nums.rindex(nums.max)}"
puts "average value : #{nums.inject{ |sum, el| sum + el }.to_f / nums.size}"
puts "average value : #{nums.reduce{ |a, b| a + b }.to_f / nums.size}"